//-----------------------------------------------------------------------------
// Software that is described herein is for illustrative purposes only  
// which provides customers with programming information regarding the  
// products. This software is supplied "AS IS" without any warranties.  
// NXP Semiconductors assumes no responsibility or liability for the 
// use of the software, conveys no license or title under any patent, 
// copyright, or mask work right to the product. NXP Semiconductors 
// reserves the right to make changes in the software without 
// notification. NXP Semiconductors also make no representation or 
// warranty that such application will be suitable for the specified 
// use without further testing or modification. 
//-----------------------------------------------------------------------------

#include "LPC17xx.h"                        /* LPC23xx definitions */

#include "sbl_config.h"
#include "comms.h"
#include "isp/isp_iap.h"
#include "board_init/board_init.h"
#include "Debug/uart.h"
#include <string.h>

const unsigned crp __attribute__((section(".ARM.__at_0x2FC"))) = CRP;

#define	BL_SHARED_MEM_MAGIC		("BOLO")
typedef struct bl_struct {
	char magic[4];
	char version[12];
	char reserved[48];
} bl_struct_t;

__attribute__ ((section(".data.$RamShared"))) bl_struct_t bl_shared;

const char version[] = "3.0.1";

void enter_isp(void)
{
  int outsync_handshake = 0;
  board_init();
  init_comms();

  /* resets when timeout occurs */
  while (1) {
    if (comm_handshake(outsync_handshake)) continue;
	outsync_handshake = isp_cmd_loop();
  }
}


/* Main Program */
int main(void)
{
  /*
   * Write bootloader version in shared memory segment so that
   * batman can read it.
   */
  strncpy(bl_shared.magic, BL_SHARED_MEM_MAGIC, 4);
  strcpy(bl_shared.version, version);

  if ( !check_isp_entry_pin() ) {
    // isp entry not requested
    execute_user_code();
  } else {
    // isp entry requested
    enter_isp();
  }

  return 1;
}
