/*
 * This is taken from the BatMan project so that we can set the MAC address
 * based on what the BatMan control app writes into the flash.
 */

#include "stdint.h"

#define	FLASH_CFG_LOCATION	0x7fc00
#define	FLASH_CFG_SIZE		256
#define FLASH_CFG_APP_NUM	6

typedef enum {
	CTL_APP = 0,
	SERIAL_APP,
	BAT_APP,
} app_type_t;

typedef struct {
	app_type_t type;
	uint16_t tcp_port;
	uint16_t udp_port;
	uint32_t baud;
	char mode[4];
	char udp_dest_ipstr[16];
} APP_FLASH_CFG;	

typedef struct {
	char macaddr[6];
	char ipstr[16];
	char gwstr[16];
	char nmstr[16];
	APP_FLASH_CFG app_cfgs[FLASH_CFG_APP_NUM];
	uint8_t chksum; /* sum of all bytes +1 to determine if entry is valid */
} FLASH_CFG;
