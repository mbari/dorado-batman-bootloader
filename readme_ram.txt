Flashmagic requests transfers to ram address 0x10000200 so if 0x10000000 is set as the start of the ram there is a potential that program memory 
gets clobbered, especially since we now also have 64 bytes of shared memory defined
at 0x10000040.

So we put the ethernet ram into the second segment at 0x2007C000 and start the main ram at 0x10001000 to avoid that problem.


