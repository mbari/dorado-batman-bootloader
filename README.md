# bootloader

This repo contains the thernet bootloader to allow UDP based-flashing of LPC1769 battery management PCB. It was imported to Bitbucket on Sep 5, 2019, from the origional Mercurial repository which lives in the MBARI shared drive, at the location, `//atlas/projectlibrary//mbari/ProjectLibrary/368000_DMO_AUV/Software/InspiredBatteryController/firmware/bootloader`

To build and flash the battery code you will need these three repos:

- [bootloader](https://bitbucket.org/mbari/dorado-batman-bootloader) (this repo, listed for verbosity)
- [lpc_open](https://bitbucket.org/mbari/dorado-lpc_open) (NXP support library for LPCxxxx MCUs)
- [batman](https://bitbucket.org/mbari/dorado-batman) (Battery management firmwarey)

Setting up MCUxpresso and importing project files
=======================================================

The source is set up as NXP MCUxpresso projects. MCUxpresso can be downloaded
from the NXP website:
https://www.nxp.com/support/developer-resources/software-development-tools/mcuxpresso-software-and-tools/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE

You will need an NXP login to download.

At the point of writing the latest version is 10.3.1 but it should be possible
to import projects into later versions of MCUxpresso.

After installing, open MCUxpresso. The first time opening it will ask to define
a workspace. This can be anywhere, most useful might be a directory called
"workspace" somewhere in your home directory.

To import the battery firmware projects, do the following:

- "File"->"Open Projects from File System"
- at "Import source" click on the "Directory..." button
- navigate to the project directory and select the "batman" directory
- click "OK"
- click "Finish"
- you should see "batman" show up in the left "Project Explorer" view

Repeat the above steps for the "lpc_open" directory and the "bootloader"
directory, the list of projects at the end should look like this:

- batman                        (battery firmware)
- bootloader                    (Ethernet bootloader)
- lpc_board_41cell              (board library for 41cell board)
- lpc_board_lincoln60           (board library for Lincoln 60 board)
- lpc_board_nxp_lpcxpresso_1769 (board library for LPCxpresso board, just used for - development purposes)
- lpc_chip_175x_6x              (chip library for LPC1769)



Building code
===================

BatMan
-------------------

The BatMan project contains two configurations, one for the Lincoln60 boards,
one for the 41 cell boards. To switch between them, right click the "batman"
project in the "Project Explorer" and select
"Build Configurations"->"Set Active"->"XXX" where XX is either "41cell" or
"Lincoln60"

To build the project, right click on "batman" in the "Project Explorer" and
select "Build Project".


Bootloader
-------------------

To build the project, right click on "bootloader" in the "Project Explorer" and
select "Build Project".


LPC_open
-------------------

LPC_Open is just a library with already built objects so doesn't need to be
built explicitely. If you make changes to the board libraries, it can be built
the same way as is described in 3.2.

Downloading the code and debugging
========================================

NOTE: To debug the BatMan code, the bootloader needs to be flashed first,
because the BatMan code will get flashed to 0x2000 not 0x0000, which means if
there is no bootloader the MCU will not start correctly.

To flash/debug the bootloader or the firmware, select the the project you want
to flash in the "Project Explorer" and click on the blue bug icon in the toolbar
at the top. This will build the code if it's not up to date and start the
LinkServer which handles communication with the debug probe.

Once you ran the degugging session for a particular project once, you can start
debugging that project from anywhere by using the green bug icon and select one
of the previously run debug configurations.

For debugging you use the LPC-Link2 debug probe. You can buy them here:
https://www.nxp.com/support/developer-resources/software-development-tools/lpc-developer-resources-/lpc-microcontroller-utilities/lpc-link2:OM13054

Sometimes the debug probe hangs or the target gets so scrambled that even a
reset of the 41cell/Lincoln60 board will fix it. Then you will have to power
cycle. If the debug probe is hung, you can try unplugging and replugging it from
the USB cable or restarting the redlink server. The easiest way to do that is to
restart the MCUxpresso IDE.

Flashing the target using Ethernet
==================================

The BatMan firmware can be flashed to the target using the Ethernet connection
to the board. For this to work the bootloader needs to be running on the target.
To get into bootloader is different on the 41cell and the Lincoln60 boards:

41cell
======

There is a reed switch on the edge of the board next to the Ethernet
connector. This reed switch needs to be magnetized while the battery is
starting up. The easiest way to achieve this is to hold a magnet next to
it while pressing the reset button on the board (when board is on a
desk) or reseting the battery with another magnet when inside a sphere.

Lincoln60
=========
The Lincoln60 board needs a special bootloader cable which will hook
it up to a reed switch (normally closed, same type as the reset reed
switch). It gets connected to J5 on the Lincoln60 daughter board.
Schematic is here:

smb://atlas/projectlibrary/368000_DMO_AUV/Schematics/CABLES/ProjectPDFsOut/battery_bootloader_cable.pdf

The pins on J1 need to be open to go into bootloader mode (shorted
for normal mode)

The LED on the 41 cell board will blink rapidly in blue once bootloader mode is
active. On the Lincoln60, the amber LED is blinking rapidly. At this point the
firmware upload can begin.

To upload the firmware you need a tool called FlashMagic from NXP, it can be
downloaded here:
(http://www.flashmagictool.com/)[http://www.flashmagictool.com/]

The tool is available for Windows only but works fine under Linux using Wine.

The bootloader code on the target runs only a rudimentary IP stack which only
supports UDP packets. This means ARP packets will get ignored and the target 
will not be discovered by just trying to send packets to the IP address.
This means for every target which needs flashing, a static ARP entry needs to
exist on the host computer. To set a static ARP address do this:

Linux: `sudo arp -s 134.89.32.121 00:0c:6a:01:03:01`

Windows: `netsh interface ipv4 add neighbors "Local Area Connection" 134.89.32.121 00-0c-6a-01-03-01`

Notes:

1. need to run as admin
2.  change `"Local Area Connection"` based on what the interface you are using
        is called

Once static ARP entries are added, start FlashMagic. In "Step 1" click
"Select..." to choose "LPC1769 Ethernet" as the target platform. Enter the IP
and MAC address of the target into the text boxes and make sure that "Interface"
points to the network device the target is connected to. Note that in order for
this to work, your computer needs to be configured for the same subnet as the
target device.

You can now test the connection by opening the "ISP" menu up top and then
"Display Memory". If everything works you should see a hex printout of the
firmware, otherwise an error dialog will pop up after a while indicating that
communications don't work.

If that works, the firmware can be uploaded to the target. In "Step 2" make 
sure the check box for "Erase blocks used by Firmware" is the only one which is
checked. Now go to "Step 3" and select the firmware. It can be found in the
project directory in the ./41cell subdirectory for the 41 cell board version or
in ./Lincoln60 for the Lincol60 version. The file to use would be 
./41cell/batman_41cell.hex or ./Lincoln60/batman_lincoln60.hex respectively.
Choose either in theFlashMagic dialog.

In "Step 4" verification of the upload can be enabled by checking the
"Verify after programming" box. The other boxes should stay unchecked. Press
the "Start" button to upload the firmware. Upload progess is shown in the
status field on the bottom.
